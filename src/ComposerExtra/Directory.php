<?php
namespace Pipaslot\ComposerExtra;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Pipaslot\Utils\Path;

/**
 * Directory management and security against URL access
 *
 * Example of using:
 * "directory": {
 *    "secure": [
 *        "relative-path"
 *    ],
 *    "create": [
 *        "relative-path"
 *    ],
 *    "empty": [
 *        "relative-path"
 *    ],
 *    "remove": [
 *        "relative-path"
 *    ]
 * }
 *
 * @author Petr Štipek <p.stipek@email.cz>
 */
class Directory implements PluginInterface, EventSubscriberInterface
{
	private static $htaccessDeny = "Order Deny,Allow\nDeny from all";



	/**
	 * Apply plugin modifications to Composer
	 *
	 * @param Composer $composer
	 * @param IOInterface $io
	 */
	public function activate(Composer $composer, IOInterface $io)
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public static function getSubscribedEvents()
	{
		return array(
			ScriptEvents::POST_INSTALL_CMD => array(
				array('run', 0)
			),
			ScriptEvents::POST_UPDATE_CMD => array(
				array('run', 0)
			),
		);
	}

	/**
	 * Run Directory extra from composer
	 * @param Event $event
	 */
	public static function run(Event $event)
	{
		$extra = $event->getComposer()->getPackage()->getExtra();
		$ins = new Directory();
		if (isset($extra['directory'])) {
			if (isset($extra['directory']['empty']) AND is_array($extra['directory']['empty'])) {
				$ins->runEmpty($extra['directory']['empty']);
			}
			if (isset($extra['directory']['create']) AND is_array($extra['directory']['create'])) {
				$ins->runCreate($extra['directory']['create']);
			}
			if (isset($extra['directory']['secure']) AND is_array($extra['directory']['secure'])) {
				$ins->runSecure($extra['directory']['secure']);
			}
			if (isset($extra['directory']['remove']) AND is_array($extra['directory']['remove'])) {
				$ins->runRemove($extra['directory']['remove']);
			}
		}
	}

	private function runEmpty(array $directories)
	{
		foreach ($directories as $directory) {
			if (!is_dir($directory)) {
				continue;
			}
			Path::emptyDirectory($directory);
		}
	}

	private function runCreate(array $directories)
	{
		foreach ($directories as $directory) {
			if (is_dir($directory)) {
				continue;
			}
			mkdir($directory);
		}
	}

	private function runSecure(array $directories)
	{
		$webConfig = file_get_contents(__DIR__ . '/web.config.xml');
		foreach ($directories as $directory) {
			if (!is_dir($directory)) {
				continue;
			}
			$path = $this->getPath($directory);
			$config = $path . "/web.config";
			if (!is_file($config)) {
				file_put_contents($config, $webConfig);
			}

			$htaccess = $path . "/.htaccess";
			if (!is_file($htaccess)) {
				file_put_contents($htaccess, self::$htaccessDeny);
			}
		}
	}

	private function runRemove(array $directories)
	{
		foreach ($directories as $directory) {
			if (!is_dir($directory)) {
				continue;
			}
			Path::remove($directory);
		}
	}

	/**
	 * @param $directory
	 * @return string Path
	 */
	private function getPath($directory)
	{
		return getcwd() . "/" . trim($directory, "\\/");
	}
}